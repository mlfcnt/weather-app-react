import React, { useState, useEffect } from "react";
import Card from "./components/Card/Card";
import "./App.css";
import Footer from "./components/Footer";
import { CSSTransitionGroup } from "react-transition-group";
import ErrorMessage from "./components/ErrorMessage";
import Header from "./components/Header";
import ViewMore from "./components/ViewMore";
import Chart from "./components/Charts/Chart";
import WeeklyChart from "./components/Charts/WeeklyChart";
import Theming from "./components/Theming";
import { Helmet } from "react-helmet";
import Geocode from "react-geocode";
import PlacesAutocomplete from "reactjs-places-autocomplete";
import { geocodeByAddress, getLatLng } from "reactjs-places-autocomplete";

const App = () => {
  const [data, setData] = useState({});
  const [selectedCity, setSelectedCity] = useState("Kathmandu");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [list, addToList] = useState([]);
  const [unique, setUnique] = useState([]);
  const [moreInfos, showMoreInfos] = useState(false);
  const [timeId, setTimeId] = useState("");
  const [weeklyChart, displayWeeklyChart] = useState(false);
  const [weeklyChartbutton, setWeeklyChartButton] = useState(
    "Graphique 5 jours"
  );
  const [theme] = useState([
    `https://bootswatch.com/4/superhero/bootstrap.min.css`,
    `https://bootswatch.com/4/cosmo/bootstrap.min.css`,
    `https://bootswatch.com/4/darkly/bootstrap.min.css`,
    `https://bootswatch.com/4/litera/bootstrap.min.css`,
    `https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/sketchy/bootstrap.min.css`,
    `https://code.divshot.com/geo-bootstrap/swatch/bootstrap.css`
  ]);
  const [selectedTheme, setSelectedTheme] = useState(
    "https://bootswatch.com/4/superhero/bootstrap.min.css"
  );
  const [latitude, setLatitude] = useState("");
  const [longitude, setLongitude] = useState("");
  const [address, setAddress] = useState("");

  useEffect(() => {
    async function getWeather() {
      const response = await fetch(
        `https://api.openweathermap.org/data/2.5/forecast?q=${selectedCity}&units=metric&lang=fr&APPID=c7d18dfd72ec3f14189f48591611c6e6`
      );
      if (response.status === 200) {
        console.log(
          "Statut Fetch : " + response.status + " " + response.statusText
        );
        const weatherData = await response.json();
        setData(weatherData);
        setLoading(false);
        addToList([...list, selectedCity]);
        setUnique([...new Set(list)]);
        displayWeeklyChart(false);
      } else {
        setError(true);
        console.log(
          "Statut Fetch : " + response.statusText + " " + response.status
        );
      }
    }
    getWeather();
    document.title = `Météo de ${selectedCity}`;
    displayError();
  }, [selectedCity]);

  const viewMore = () => {
    if (moreInfos) {
      return (
        <>
          <ViewMore data={data} timeId={timeId} />{" "}
          <div className="p-4">
            <Chart data={data} timeId={timeId} />
          </div>
        </>
      );
    } else {
      return null;
    }
  };

  const handleTheme = num => {
    setSelectedTheme(theme[num]);
  };

  const handleMoreInfos = timeId => {
    showMoreInfos(true);
    setTimeId(timeId);
    console.log(timeId);
  };
  let dropdown;

  if (unique.length > 0) {
    dropdown = (
      <>
        <div className="recherches">
          <div className="dropdown show">
            <a
              className="btn btn-secondary dropdown-toggle"
              role="button"
              href="/#"
              id="dropdownMenuLink"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Historique
            </a>

            <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
              {unique.map(element => (
                <a
                  key={unique.indexOf({ element }.element)}
                  className="dropdown-item"
                  onClick={() => setSelectedCity({ element }.element)}
                  href="/#"
                >
                  {element}
                </a>
              ))}
            </div>
          </div>
        </div>
      </>
    );
  } else {
    dropdown = null;
  }

  const weeklyGraph = () => {
    if (weeklyChart)
      return (
        <div className="p-4">
          <WeeklyChart data={data} timeId={timeId} />
        </div>
      );
  };

  const handleDisplayWeeklyChart = () => {
    displayWeeklyChart(!weeklyChart);
    if (weeklyChart) {
      setWeeklyChartButton("Graphique 5 jours");
    } else {
      setWeeklyChartButton("Masquer");
    }
  };

  const options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };
  const getCoords = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        Geocode.setApiKey("AIzaSyAmJrLSFg9dtb8Xzw1MrXx52O-Jl4ukBjQ");
        setLatitude(position.coords.latitude);
        setLongitude(position.coords.longitude);
        console.log(latitude, longitude);
        Geocode.fromLatLng(latitude, longitude).then(
          response => {
            const address = response.results[0].address_components[2].long_name;
            console.log(address);
            setSelectedCity(address);
          },
          error => {
            console.error(error);
          },
          { options }
        );
      },
      err => console.log(err)
    );
  };

  const displayError = () => {
    return (
      <div className="text-center">
        <ErrorMessage selectedCity={selectedCity} />
      </div>
    );
  };

  const handleChange = address => {
    setAddress(address);
  };

  const handleSelect = address => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => console.log("Success", latLng))
      .catch(error => console.error("Error", error))
      .then(setSelectedCity(address))
      .then(setAddress(""));
  };

  const searchOptions = {};

  if (loading) {
    return (
      <>
        <Helmet>
          <link
            rel="stylesheet"
            href="https://bootswatch.com/4/superhero/bootstrap.min.css"
          />
        </Helmet>
        <h2 className="text-center m-4">Récupération des données...</h2>
      </>
    );
  } else {
    return (
      <div className="border-5 border-primary">
        <div className="row d-flex justify-content-between mx-4 mt-4">
          <div className="col-sm-4 col-xs-12 mb-4">
            {/*! THEME */}
            <Theming selectedTheme={selectedTheme} />

            <div className="dropdown">
              <button
                className="btn btn-secondary dropdown-toggle btn-lg"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <span>
                  <i className="fas fa-palette fa-lg" />
                </span>
              </button>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <a
                  className="dropdown-item"
                  href="/#"
                  onClick={() => handleTheme(0)}
                >
                  Superman (dark)
                </a>
                <a
                  className="dropdown-item"
                  href="/#"
                  onClick={() => handleTheme(1)}
                >
                  Cosmo (light)
                </a>
                <a
                  className="dropdown-item"
                  href="/#"
                  onClick={() => handleTheme(2)}
                >
                  Darkly (dark)
                </a>
                <a
                  className="dropdown-item"
                  href="/#"
                  onClick={() => handleTheme(3)}
                >
                  Litera (light)
                </a>
                <a
                  className="dropdown-item"
                  href="/#"
                  onClick={() => handleTheme(4)}
                >
                  Sketchy (light)
                </a>
                <a
                  className="dropdown-item"
                  href="/#"
                  onClick={() => handleTheme(5)}
                >
                  Jordi
                </a>
              </div>
            </div>
            {/* FIN THEME */}
          </div>
          <div className="col-sm-4 col-xs-12 mx-auto">
            <Header city={data.city.name} country={data.city.country} />
          </div>
          <div className="col-sm-4 col-xs-12 text-center">
            <button
              type="button"
              className="btn btn-secondary btn-lg float-sm-right mt-4"
              onClick={() => getCoords()}
            >
              Géolocalisation{" "}
            </button>
          </div>
        </div>

        <div>
          {/* FORM */}
          <br /> <br />
          <div className="text-center m-4">
            <PlacesAutocomplete
              value={address}
              onChange={handleChange}
              onSelect={handleSelect}
              searchOptions={searchOptions}
            >
              {({
                getInputProps,
                suggestions,
                getSuggestionItemProps,
                loading
              }) => (
                <div>
                  <input
                    {...getInputProps({
                      placeholder: "Chercher une ville...",
                      className: "location-search-input"
                    })}
                  />
                  <div className="autocomplete-dropdown-container">
                    {loading && <div>Loading...</div>}
                    {suggestions.map(suggestion => {
                      const className = suggestion.active
                        ? "suggestion-item--active"
                        : "suggestion-item";
                      const style = suggestion.active
                        ? { color: "orange", cursor: "pointer" }
                        : { cursor: "pointer" };
                      return (
                        <div
                          {...getSuggestionItemProps(suggestion, {
                            className,
                            style
                          })}
                        >
                          <span>{suggestion.description}</span>
                        </div>
                      );
                    })}
                  </div>
                </div>
              )}
            </PlacesAutocomplete>
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <CSSTransitionGroup
              transitionName="example"
              transitionEnterTimeout={500}
              transitionLeaveTimeout={300}
            >
              {dropdown}
            </CSSTransitionGroup>
          </div>
          {/* ENDFORM */}
          {error && displayError()}
          {/* <p className="text-center">A 15h car API gratuite è_é</p> */}
          <div className="row m-4 justify-content-center eq-width eq-height">
            {/* CARD */}
            <div
              className="border hover col-xs-6 m-4 p-3 text-center"
              onClick={() => handleMoreInfos(0)}
            >
              <Card
                date={data.list[0].dt}
                thumbnail={data.list[0].weather[0].icon}
                temp={data.list[0].main.temp}
              />
            </div>
            {/* ENDCARD */}

            {/* CARD */}
            <div
              className="border hover col-xs-6 m-4 p-3 text-center"
              onClick={() => handleMoreInfos(8)}
            >
              <Card
                date={data.list[8].dt}
                thumbnail={data.list[8].weather[0].icon}
                temp={data.list[8].main.temp}
              />
            </div>
            {/* ENDCARD */}

            {/* CARD */}
            <div
              className="border hover col-xs-6 m-4 p-3 text-center"
              onClick={() => handleMoreInfos(16)}
            >
              <Card
                date={data.list[16].dt}
                thumbnail={data.list[16].weather[0].icon}
                temp={data.list[16].main.temp}
              />
            </div>
            {/* ENDCARD */}

            {/* CARD */}
            <div
              className="border hover col-xs-6 m-4 p-3 text-center"
              onClick={() => handleMoreInfos(24)}
            >
              <Card
                date={data.list[24].dt}
                thumbnail={data.list[24].weather[0].icon}
                temp={data.list[24].main.temp}
              />
            </div>
            {/* ENDCARD */}

            {/* CARD */}
            <div
              className="border hover col-xs-6 m-4 p-3 text-center"
              onClick={() => handleMoreInfos(32)}
            >
              <Card
                date={data.list[32].dt}
                thumbnail={data.list[32].weather[0].icon}
                temp={data.list[32].main.temp}
              />
            </div>

            {/* ENDCARD */}
          </div>
          <div className="text-center">
            <CSSTransitionGroup
              transitionName="example"
              transitionEnterTimeout={500}
              transitionLeaveTimeout={300}
            >
              {!moreInfos && (
                <p className=" m-4 text-muted muted-info">
                  Cliquez sur un jour pour afficher plus de données
                </p>
              )}
            </CSSTransitionGroup>
          </div>
          <div className="text-center">
            <button
              type="button"
              className="btn btn-info"
              onClick={() => handleDisplayWeeklyChart()}
            >
              {weeklyChartbutton}
            </button>
          </div>
          {weeklyGraph()}
          <hr />
          <div className="text-center">
            <CSSTransitionGroup
              transitionName="example"
              transitionEnterTimeout={500}
              transitionLeaveTimeout={300}
            >
              {moreInfos && (
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => showMoreInfos(false)}
                >
                  Masquer les détails
                </button>
              )}
              {viewMore()}
            </CSSTransitionGroup>
          </div>
          <Footer />
        </div>
      </div>
    );
  }
};

export default App;

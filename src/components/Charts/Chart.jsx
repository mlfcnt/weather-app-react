import React, { useState } from "react";
import { Line } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";

const Chart = props => {
  const formattedDate = diff => {
    var dateToFormat = new Date(props.data.list[diff].dt * 1000);
    var hours = dateToFormat.getHours();
    var minutes = "0" + dateToFormat.getMinutes();
    var formattedTime = hours + ":" + minutes.substr(-2);

    return formattedTime;
  };

  const [dataLine] = useState({
    labels: [
      formattedDate(0),
      formattedDate(1),
      formattedDate(2),
      formattedDate(3),
      formattedDate(4),
      formattedDate(5),
      formattedDate(6),
      formattedDate(7)
    ],
    datasets: [
      {
        label: "Température (°C)",
        fill: false,
        lineTension: 0.3,
        backgroundColor: "rgba(225, 204,230, .3)",
        borderColor: "white",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgb(205, 130,1 58)",
        pointBackgroundColor: "rgb(255, 255, 255)",
        pointBorderWidth: 10,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgb(0, 0, 0)",
        pointHoverBorderColor: "rgba(220, 220, 220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [
          props.data.list[props.timeId].main.temp,
          props.data.list[props.timeId + 1].main.temp,
          props.data.list[props.timeId + 2].main.temp,
          props.data.list[props.timeId + 3].main.temp,
          props.data.list[props.timeId + 4].main.temp,
          props.data.list[props.timeId + 5].main.temp,
          props.data.list[props.timeId + 6].main.temp,
          props.data.list[props.timeId + 7].main.temp
        ]
      },
      {
        label: "Vent (Km/h)",
        fill: false,
        lineTension: 0.3,
        backgroundColor: "orange",
        borderColor: "orange",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgb(35, 26, 136)",
        pointBackgroundColor: "rgb(255, 255, 255)",
        pointBorderWidth: 10,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgb(0, 0, 0)",
        pointHoverBorderColor: "rgba(220, 220, 220, 1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [
          props.data.list[props.timeId].wind.speed * 3.6,
          props.data.list[props.timeId + 1].wind.speed * 3.6,
          props.data.list[props.timeId + 2].wind.speed * 3.6,
          props.data.list[props.timeId + 3].wind.speed * 3.6,
          props.data.list[props.timeId + 4].wind.speed * 3.6,
          props.data.list[props.timeId + 5].wind.speed * 3.6,
          props.data.list[props.timeId + 6].wind.speed * 3.6,
          props.data.list[props.timeId + 7].wind.speed * 3.6
        ]
      },
      {
        label: "Nuages (%)",
        fill: false,
        lineTension: 0.3,
        backgroundColor: "blue",
        borderColor: "blue",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgb(35, 26, 136)",
        pointBackgroundColor: "rgb(255, 255, 255)",
        pointBorderWidth: 10,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgb(0, 0, 0)",
        pointHoverBorderColor: "rgba(220, 220, 220, 1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [
          props.data.list[props.timeId].clouds.all,
          props.data.list[props.timeId + 1].clouds.all,
          props.data.list[props.timeId + 2].clouds.all,
          props.data.list[props.timeId + 3].clouds.all,
          props.data.list[props.timeId + 4].clouds.all,
          props.data.list[props.timeId + 5].clouds.all,
          props.data.list[props.timeId + 6].clouds.all,
          props.data.list[props.timeId + 7].clouds.all
        ]
      }
    ]
  });
  return (
    <div>
      <MDBContainer className="bg-info text-dark px-4 py-1">
        <Line data={dataLine} options={{ responsive: true }} />
      </MDBContainer>
    </div>
  );
};

export default Chart;

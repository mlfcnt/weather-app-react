import React, { useState } from "react";
import { Line } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";

const WeeklyChart = props => {
  const formattedDate = diff => {
    var timestamp = props.data.list[diff].dt;
    var a = new Date(timestamp * 1000);
    var days = [
      "Dimanche",
      "Lundi",
      "Mardi",
      "Mercredi",
      "Jeudi",
      "Vendredi",
      "Samedi"
    ];
    var dayOfWeek = days[a.getDay()];
    return dayOfWeek;
  };

  const [dataLine] = useState({
    labels: [
      formattedDate(0),
      formattedDate(8),
      formattedDate(16),
      formattedDate(24),
      formattedDate(32)
    ],
    datasets: [
      {
        label: "Température (°C)",
        fill: true,
        lineTension: 0.3,
        backgroundColor: "rgba(225, 204,230, .3)",
        borderColor: "white",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgb(205, 130,1 58)",
        pointBackgroundColor: "rgb(255, 255, 255)",
        pointBorderWidth: 10,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgb(0, 0, 0)",
        pointHoverBorderColor: "rgba(220, 220, 220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [
          props.data.list[0].main.temp,
          props.data.list[8].main.temp,
          props.data.list[16].main.temp,
          props.data.list[24].main.temp,
          props.data.list[32].main.temp
        ]
      },
      {
        label: "Vent (Km/h)",
        fill: true,
        lineTension: 0.3,
        backgroundColor: "orange",
        borderColor: "orange",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgb(35, 26, 136)",
        pointBackgroundColor: "rgb(255, 255, 255)",
        pointBorderWidth: 10,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgb(0, 0, 0)",
        pointHoverBorderColor: "rgba(220, 220, 220, 1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [
          props.data.list[0].wind.speed * 3.6,
          props.data.list[8].wind.speed * 3.6,
          props.data.list[16].wind.speed * 3.6,
          props.data.list[24].wind.speed * 3.6,
          props.data.list[32].wind.speed * 3.6
        ]
      },
      {
        label: "Nuages (%)",
        fill: false,
        lineTension: 0.3,
        backgroundColor: "blue",
        borderColor: "blue",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgb(35, 26, 136)",
        pointBackgroundColor: "rgb(255, 255, 255)",
        pointBorderWidth: 10,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgb(0, 0, 0)",
        pointHoverBorderColor: "rgba(220, 220, 220, 1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [
          props.data.list[0].clouds.all,
          props.data.list[1].clouds.all,
          props.data.list[2].clouds.all,
          props.data.list[3].clouds.all,
          props.data.list[4].clouds.all,
          props.data.list[5].clouds.all,
          props.data.list[6].clouds.all,
          props.data.list[7].clouds.all
        ]
      }
    ]
  });
  return (
    <div>
      <MDBContainer className="px-4 py-1 mt-4">
        <Line data={dataLine} options={{ responsive: true }} />
      </MDBContainer>
    </div>
  );
};

export default WeeklyChart;

import React from "react";
import { Helmet } from "react-helmet";

const Theming = props => {
  return (
    <div className="application">
      <Helmet>
        <link rel="stylesheet" href={props.selectedTheme} />
      </Helmet>
    </div>
  );
};

export default Theming;

import React from "react";

const Footer = () => {
  return (
    <footer className="page-footer font-small blue">
      <div className="footer-copyright text-center py-3">
        <a
          href="https://gitlab.com/mlfcnt/weather-app-react"
          target="_blank"
          rel="noopener noreferrer"
        >
          <span
            role="img"
            aria-label="fox emoji"
            data-toggle="tooltip"
            title="Voir le code sur gitlab"
          >
            🦊
          </span>{" "}
        </a>
      </div>
    </footer>
  );
};

export default Footer;

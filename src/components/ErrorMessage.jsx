import React from 'react'
import FlashMessage from "react-flash-message";


const ErrorMessage = props => {
    return (
        <>
        <FlashMessage duration={5000} persistOnHover={false}>
          <p>
            <i
              class="fas fa-exclamation-triangle"
              style={{ color: "#df691a" }}
            />
            <strong>
              "<span> {props.selectedCity}</span>" n'existe pas.
            </strong>
          </p>
        </FlashMessage>
      </>
    )
}

export default ErrorMessage

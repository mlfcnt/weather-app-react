import React from "react";

const Thumbnail = props => {
  const icon = `http://openweathermap.org/img/wn/${props.thumbnail}@2x.png`;
  return (
    <>
      <img className="mx-auto" src={icon} alt="" />
    </>
  );
};

export default Thumbnail;

import React from "react";

const Temp = props => {
  return (
    <div>
      <span>
        Température :{" "}
        <span className="temperature text-primary">
          {" "}
          <strong>{props.temp}°C</strong>{" "}
        </span>
      </span>
    </div>
  );
};

export default Temp;

import React from "react";

const MaxTemp = props => {
  return <span>Max : {props.tempMax}°C</span>;
};

export default MaxTemp;

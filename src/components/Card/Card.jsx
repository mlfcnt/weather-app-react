import React from "react";
import Day from "./Day";
import Thumbnail from "./Thumbnail";
import Temp from "./Temp";

const Card = props => {
  return (
    <>
      <Day date={props.date} />
      <Thumbnail thumbnail={props.thumbnail} />
      {/* <MinTemp tempMin={props.tempMin} /> - <MaxTemp tempMax={props.tempMax} /> */}
      <Temp temp={props.temp} />
    </>
  );
};

export default Card;

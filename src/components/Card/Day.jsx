import React from "react";
import Moment from "react-moment";
import "moment/locale/fr";

const Day = props => {
  const date = (
    <Moment unix format="dddd D">
      {props.date}
    </Moment>
  );
  return (
    <>
      <span className="width capitalize thick">{date}</span>
    </>
  );
};

export default Day;

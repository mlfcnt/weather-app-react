import React from "react";

const MinTemp = props => {
  return <span className="">Min : {props.tempMin}°C</span>;
};

export default MinTemp;

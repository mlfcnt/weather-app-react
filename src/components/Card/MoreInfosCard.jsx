import React from "react";
import Thumbnail from "./Thumbnail";
import Temp from "./Temp";
import Moment from "react-moment";
import "moment/locale/fr";
import MinTemp from "./MinTemp";
import MaxTemp from "./MaxTemp";

const MoreInfosCard = props => {
  const date = (
    <Moment unix format="HH:mm">
      {props.date}
    </Moment>
  );
  return (
    <div>
      <h3 className="pt-2">
        <strong>{date}</strong>
      </h3>
      <hr className="w-75 bg-light" />
      <div className="px-4">
        <Thumbnail thumbnail={props.thumbnail} />
        <Temp temp={props.temp} />
        <MinTemp tempMin={props.tempMin} /> -{" "}
        <MaxTemp tempMax={props.tempMax} />
        <p>Humidité : {props.humidity}</p>
        <p className="capitalize">{props.description}</p>
      </div>
    </div>
  );
};

export default MoreInfosCard;

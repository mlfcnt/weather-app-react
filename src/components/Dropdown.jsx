import React from "react";

const Dropdown = props => {
  let dropdown = "";

  return (
    <div>
      if (props.unique.length > 0 && props.display){" "}
      {
        (dropdown = (
          <>
            <div className="recherches">
              <div className="dropdown show">
                <a
                  className="btn btn-secondary dropdown-toggle"
                  role="button"
                  href="/#"
                  id="dropdownMenuLink"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  col-xs-6
                >
                  Recherches précédentes
                </a>

                <div
                  className="dropdown-menu"
                  aria-labelledby="dropdownMenuLink"
                >
                  {props.unique.map(element => (
                    <a
                      key={props.unique.indexOf({ element }.element)}
                      className="dropdown-item"
                      onClick={() => props.setSelectedCity({ element }.element)}
                      href="/#"
                    >
                      {element}
                      <button
                        type="button"
                        class="btn btn-danger btn-sm ml-2"
                        onClick={console.log(element)}
                      >
                        X
                      </button>
                    </a>
                  ))}
                </div>
                <button
                  type="button"
                  class="btn btn-outline-danger btn-sm ml-2"
                  onClick={() => props.toDisplay(false)}
                >
                  🙈
                </button>
              </div>
            </div>
          </>
        ))
      }{" "}
      else {(dropdown = null)}
    </div>
  );
};

export default Dropdown;

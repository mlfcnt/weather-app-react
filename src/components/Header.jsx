import React from "react";
import Flag from "react-world-flags";

const Header = props => {
  return (
    <>
      <h1 className="col-12 text-center header">
        Météo de {props.city}
        <Flag code={props.country} height="25" className="m-2" />
      </h1>
    </>
  );
};

export default Header;

import React, { useState } from "react";
import NumberFormat from "react-number-format";
import MoreInfosCard from "./Card/MoreInfosCard";
import Moment from "react-moment";
import "moment/locale/fr";
// import Modal from "react-modal";
import Modal from "react-bootstrap/Modal";

const ViewMore = props => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const date = (
    <Moment unix format="dddd Do MMMM YYYY">
      {props.data.list[props.timeId].dt}
    </Moment>
  );

  return (
    <>
      <h4 className="m-4">
        Météo de{" "}
        <a
          href="/# "
          onClick={handleShow}
          data-toggle="tooltip"
          title="Plus d'infos sur la ville"
        >
          {props.data.city.name},{props.data.city.country}{" "}
        </a>{" "}
        pour le <span className="capitalize">{date} </span>
      </h4>
      <div className="tempSemaine">
        <div className="row m-4 justify-content-center eq-width eq-height">
          <div className="card col-xs-6 m-4 p-2 text-center bg-secondary">
            <MoreInfosCard
              date={props.data.list[props.timeId].dt}
              thumbnail={props.data.list[props.timeId].weather[0].icon}
              temp={props.data.list[props.timeId].main.temp}
              tempMin={props.data.list[props.timeId].main.temp_min}
              tempMax={props.data.list[props.timeId].main.temp_max}
              humidity={props.data.list[props.timeId].main.humidity}
              description={props.data.list[props.timeId].weather[0].description}
            />
          </div>
          <div className="card col-xs-6 m-4 p-2 text-center bg-secondary">
            <MoreInfosCard
              date={props.data.list[props.timeId + 1].dt}
              thumbnail={props.data.list[props.timeId + 1].weather[0].icon}
              temp={props.data.list[props.timeId + 1].main.temp}
              tempMin={props.data.list[props.timeId + 1].main.temp_min}
              tempMax={props.data.list[props.timeId + 1].main.temp_max}
              humidity={props.data.list[props.timeId + 1].main.humidity}
              description={
                props.data.list[props.timeId + 1].weather[0].description
              }
            />
          </div>
          <div className="card col-xs-6 m-4 p-2 text-center bg-secondary">
            <MoreInfosCard
              date={props.data.list[props.timeId + 2].dt}
              thumbnail={props.data.list[props.timeId + 2].weather[0].icon}
              temp={props.data.list[props.timeId + 2].main.temp}
              tempMin={props.data.list[props.timeId + 2].main.temp_min}
              tempMax={props.data.list[props.timeId + 2].main.temp_max}
              humidity={props.data.list[props.timeId + 2].main.humidity}
              description={
                props.data.list[props.timeId + 2].weather[0].description
              }
            />
          </div>
          <div className="card col-xs-6 m-4 p-2 text-center bg-secondary">
            <MoreInfosCard
              date={props.data.list[props.timeId + 3].dt}
              thumbnail={props.data.list[props.timeId + 3].weather[0].icon}
              temp={props.data.list[props.timeId + 3].main.temp}
              tempMin={props.data.list[props.timeId + 3].main.temp_min}
              tempMax={props.data.list[props.timeId + 3].main.temp_max}
              humidity={props.data.list[props.timeId + 3].main.humidity}
              description={
                props.data.list[props.timeId + 3].weather[0].description
              }
            />
          </div>
          <div className="card col-xs-6 m-4 p-2 text-center bg-secondary">
            <MoreInfosCard
              date={props.data.list[props.timeId + 4].dt}
              thumbnail={props.data.list[props.timeId + 4].weather[0].icon}
              temp={props.data.list[props.timeId + 4].main.temp}
              tempMin={props.data.list[props.timeId + 4].main.temp_min}
              tempMax={props.data.list[props.timeId + 4].main.temp_max}
              humidity={props.data.list[props.timeId + 4].main.humidity}
              description={
                props.data.list[props.timeId + 4].weather[0].description
              }
            />
          </div>
          <div className="card col-xs-6 m-4 p-2 text-center bg-secondary">
            <MoreInfosCard
              date={props.data.list[props.timeId + 5].dt}
              thumbnail={props.data.list[props.timeId + 5].weather[0].icon}
              temp={props.data.list[props.timeId + 5].main.temp}
              tempMin={props.data.list[props.timeId + 5].main.temp_min}
              tempMax={props.data.list[props.timeId + 5].main.temp_max}
              humidity={props.data.list[props.timeId + 5].main.humidity}
              description={
                props.data.list[props.timeId + 5].weather[0].description
              }
            />
          </div>
          <div className="card col-xs-6 m-4 p-2 text-center bg-secondary">
            <MoreInfosCard
              date={props.data.list[props.timeId + 6].dt}
              thumbnail={props.data.list[props.timeId + 6].weather[0].icon}
              temp={props.data.list[props.timeId + 6].main.temp}
              tempMin={props.data.list[props.timeId + 6].main.temp_min}
              tempMax={props.data.list[props.timeId + 6].main.temp_max}
              humidity={props.data.list[props.timeId + 6].main.humidity}
              description={
                props.data.list[props.timeId + 6].weather[0].description
              }
            />
          </div>
          <div className="card col-xs-6 m-4 p-2 text-center bg-secondary">
            <MoreInfosCard
              date={props.data.list[props.timeId + 7].dt}
              thumbnail={props.data.list[props.timeId + 7].weather[0].icon}
              temp={props.data.list[props.timeId + 7].main.temp}
              tempMin={props.data.list[props.timeId + 7].main.temp_min}
              tempMax={props.data.list[props.timeId + 7].main.temp_max}
              humidity={props.data.list[props.timeId + 7].main.humidity}
              description={
                props.data.list[props.timeId + 7].weather[0].description
              }
            />
          </div>
        </div>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title className="mx-auto">{props.data.city.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Population :{" "}
            <NumberFormat
              value={props.data.city.population}
              displayType={"text"}
              thousandSeparator={true}
            />{" "}
            habitants
          </p>
          <p>
            Altitude :{" "}
            {(props.data.list[props.timeId].main.grnd_level / 3.2808).toFixed(
              2
            )}{" "}
            mètres
          </p>
          <p>Latitude : {props.data.city.coord.lat}</p>
          <p>Longitude : {props.data.city.coord.lon}</p>
        </Modal.Body>
        <Modal.Footer>
          <button
            type="button"
            className="btn btn-primary"
            onClick={handleClose}
          >
            Fermer
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ViewMore;
